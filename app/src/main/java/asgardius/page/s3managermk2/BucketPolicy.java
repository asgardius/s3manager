package asgardius.page.s3managermk2;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.S3ClientOptions;

public class BucketPolicy extends AppCompatActivity {
    String username, password, endpoint, bucket, location;
    Region region;
    S3ClientOptions s3ClientOptions;
    AWSCredentials myCredentials;
    AmazonS3 s3client;
    boolean style, publicbucket;
    ProgressBar simpleProgressBar;
    TextView permission;
    Button setpublic, setprivate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bucket_policy);
        simpleProgressBar = (ProgressBar) findViewById(R.id.simpleProgressBar);
        permission = (TextView) findViewById(R.id.permission);
        endpoint = getIntent().getStringExtra("endpoint");
        username = getIntent().getStringExtra("username");
        password = getIntent().getStringExtra("password");
        bucket = getIntent().getStringExtra("bucket");
        style = getIntent().getBooleanExtra("style", false);
        location = getIntent().getStringExtra("region");
        getSupportActionBar().setTitle(bucket+"/");
        region = Region.getRegion(location);
        s3ClientOptions = S3ClientOptions.builder().build();
        myCredentials = new BasicAWSCredentials(username, password);
        try {
            s3client = new AmazonS3Client(myCredentials, region);
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_SHORT).show();
        }
        s3client.setEndpoint(endpoint);
        s3ClientOptions.setPathStyleAccess(style);
        s3client.setS3ClientOptions(s3ClientOptions);
        setprivate = (Button)findViewById(R.id.set_private);
        setpublic = (Button)findViewById(R.id.set_public);
        Thread getPolicy = new Thread(new Runnable() {

            @Override
            public void run() {
                try  {
                    //Your code goes here
                    String policy = s3client.getBucketPolicy(bucket).getPolicyText();
                    publicbucket = policy.contains("arn:aws:s3:::"+bucket+"/*") && policy.contains("s3:GetObject");
                    //System.out.println(policy);

                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            //Your code goes here
                            if(publicbucket){
                                permission.setText(getResources().getString(R.string.public_bucket));
                            } else {
                                permission.setText(getResources().getString(R.string.custom_policy));
                                setpublic.setVisibility(View.VISIBLE);
                            }
                            setprivate.setVisibility(View.VISIBLE);
                            simpleProgressBar.setVisibility(View.INVISIBLE);
                        }
                    });

                } catch (Exception e) {
                    e.printStackTrace();
                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            if(e.toString().contains("NoSuchBucketPolicy")) {
                                permission.setText(getResources().getString(R.string.private_bucket));
                                setpublic.setVisibility(View.VISIBLE);
                                simpleProgressBar.setVisibility(View.INVISIBLE);
                            }
                            else {
                                Toast.makeText(getApplicationContext(),e.toString(), Toast.LENGTH_SHORT).show();
                                finish();
                            }
                        }
                    });
                    //Toast.makeText(getApplicationContext(),getResources().getString(R.string.media_list_fail), Toast.LENGTH_SHORT).show();
                }
            }
        });
        getPolicy.start();
        setpublic.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                //buttonaction
                simpleProgressBar.setVisibility(View.VISIBLE);
                setPublic();
            }
        });
        setprivate.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                //buttonaction
                simpleProgressBar.setVisibility(View.VISIBLE);
                setPrivate();
            }
        });
    }
    private void setPublic() {
        Thread setPublic = new Thread(new Runnable() {

            @Override
            public void run() {
                try  {
                    //Your code goes here
                    String policy = "{\"Version\":\"2012-10-17\",\"Statement\":[{\"Effect\":\"Allow\",\"Principal\":{\"AWS\":[\"*\"]},\"Action\":[\"s3:GetBucketLocation\",\"s3:ListBucket\"],\"Resource\":[\"arn:aws:s3:::"
                            +bucket+"\"]},{\"Effect\":\"Allow\",\"Principal\":{\"AWS\":[\"*\"]},\"Action\":[\"s3:GetObject\"],\"Resource\":[\"arn:aws:s3:::"+bucket+"/*\"]}]}";
                    s3client.setBucketPolicy(bucket, policy);

                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(),getResources().getString(R.string.policy_ok), Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    });

                } catch (Exception e) {
                    e.printStackTrace();
                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            simpleProgressBar.setVisibility(View.INVISIBLE);
                            Toast.makeText(getApplicationContext(),getResources().getString(R.string.policy_error), Toast.LENGTH_SHORT).show();
                        }
                    });
                    //Toast.makeText(getApplicationContext(),getResources().getString(R.string.media_list_fail), Toast.LENGTH_SHORT).show();
                }
            }
        });
        setPublic.start();
    }

    private void setPrivate() {
        Thread setPrivate = new Thread(new Runnable() {

            @Override
            public void run() {
                try  {
                    //Your code goes here
                    s3client.deleteBucketPolicy(bucket);

                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(),getResources().getString(R.string.policy_ok), Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    });

                } catch (Exception e) {
                    e.printStackTrace();
                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            simpleProgressBar.setVisibility(View.INVISIBLE);
                            Toast.makeText(getApplicationContext(),getResources().getString(R.string.policy_error), Toast.LENGTH_SHORT).show();
                        }
                    });
                    //Toast.makeText(getApplicationContext(),getResources().getString(R.string.media_list_fail), Toast.LENGTH_SHORT).show();
                }
            }
        });
        setPrivate.start();
    }
}